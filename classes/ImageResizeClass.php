<?php
// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;
use WP_CLI\Utils;

class ImageResize{
    
    protected $attachment;
    protected $meta;
    protected $images;
    protected $original_image;
    
    protected static $default_run_command_options = array(
              'launch'     => true, // Launch a new process, or reuse the existing.
            'exit_error' => false, // Exit on error by default.
            'return'     => true, // Capture and return output, or render in realtime.
            'parse'      => 'json' // Parse returned output as a particular format.
    );
        
    protected static $image_keys = ['width' => null, 'height' => null, 'file' => null, 'mime-type' => null];
        
    public function __construct($attachment_id, $mime_type){
        
        $this->attachment_id = $attachment_id;
        $this->mime_type = $mime_type;
    
        WP_CLI::log("post meta get $attachment_id _wp_attachment_metadata --format=json");
        
        $this->meta = WP_CLI::runcommand("post meta get $attachment_id _wp_attachment_metadata --format=json", Self::$default_run_command_options);

        if(empty($this->meta)){
            throw new \Exception('no meta available');
        }

        $this->wp_install_path = get_home_path();
        $this->uploads_path = $this->wp_install_path . 'wp-content/uploads/';

        $this->normalizeOriginalImage();

        WP_CLI::log( $this->mime_type );
        WP_CLI::log($this->original_image->file);
        
        return true;
    }
    
    public function normalizeOriginalImage(){
        //extract file paths from meta
        $this->original_image = (object) array_intersect_key($this->meta, Self::$image_keys);
        $this->original_image->file = $this->uploads_path . $this->original_image->file;
        $this->path_info = pathinfo($this->original_image->file);
        $this->original_image->width = intval($this->original_image->width);
        $this->original_image->height = intval($this->original_image->height);
    }
    
    public function resizeWpApproach($max_width, $max_height){
        //only handle jpg for now. and make sure the image exists
        
        if(!in_array($this->mime_type, ['image/jpeg']) || !file_exists($this->original_image->file)){
            //get outta dodge yo
            WP_CLI::log( sprintf( 'Skipping image %s...', $this->original_image->file ) );
            return false;
        }
        
        if($this->original_image->width > $max_width || $this->original_image->height > $max_height){
            
            WP_CLI::log("Width of {$this->original_image->width} and height of {$this->original_image->height} need resize.");
            
            //figure out if we're dealing with width or height
            $largest_dimension = $this->original_image->width > $this->original_image->height ? 'width' : 'height';
            
            //compute scale
            switch($largest_dimension){
                case 'width':
                    $scale_by = (float) $max_width / $this->original_image->width;
                break;               
                case 'height':
                    $scale_by = (float) $max_height / $this->original_image->height;
                break;                
            }
            
            //get new sizes
            $resize_to_width = floor($scale_by * $this->original_image->width);
            $resize_to_height = floor($scale_by * $this->original_image->height);
            WP_CLI::log("Processing {$this->original_image->file}:");
            
            $og_image = Image::make($this->original_image->file);
            $og_size = $og_image->filesize();

            $image_editor = wp_get_image_editor($this->original_image->file);
            if (is_wp_error($image_editor)) {
                throw new Exception("Issue creating image editor to resize");   
            }
            $result = $image_editor->resize($resize_to_width, $resize_to_height);
            if (!is_wp_error($result)) {
                $image_editor->save($this->original_image->file);
            }
            else{
                throw new Exception("Issue resizing image using wp image editor");
            }     
            
            Self::updateAttachmentMeta($this->attachment_id, $this->original_image->file);       
                                                
            // $rszd_image = $og_image->resize($resize_to_width, $resize_to_height);
                        
            // //save
            // $rszd_image->save($this->original_image->file, 100);   
            
            // $rszd_image_size = $rszd_image->filesize();
            
            $rszd_size = filesize($this->original_image->file);
            
            $saved = 100 - (int) $rszd_size / $og_size * 100;
            WP_CLI::log("Resize complete. Reduced by {$saved}%");
            
            
        }
        
        else{
            WP_CLI::log("Width of {$this->original_image->width} and height of {$this->original_image->height} do not qualify for resize.");
        }
    }
    
    public function resizeMattApproach($max_width, $max_height){
        //only handle jpg for now. and make sure the image exists
        
        if(!in_array($this->mime_type, ['image/jpeg']) || !file_exists($this->original_image->file)){
            //get outta dodge yo
            WP_CLI::log( sprintf( 'Skipping image %s...', $this->original_image->file ) );
            return false;
        }
        
        if($this->original_image->width > $max_width || $this->original_image->height > $max_height){
            
            WP_CLI::log("Width of {$this->original_image->width} and height of {$this->original_image->height} need resize.");
            
            //figure out who the offender is.
            if($this->original_image->width > $max_width && $this->original_image->height > $max_height){
                $largest_dimension = $this->original_image->width > $this->original_image->height ? 'width' : 'height';    
            }
            else if($this->original_image->width > $max_width){
                $largest_dimension = 'width';
            }
            else{
                $largest_dimension = 'height';
            }
                            
            WP_CLI::log("Offending Largest Dimension is $largest_dimension");
            
            //compute scale
            switch($largest_dimension){
                case 'width':
                    $scale_by = (float) $max_width / $this->original_image->width;
                break;               
                case 'height':
                    $scale_by = (float) $max_height / $this->original_image->height;
                break;                
            }
            
            WP_CLI::log("Scale BY = $scale_by");
            
            //get new sizes
            $resize_to_width = floor($scale_by * $this->original_image->width);
            $resize_to_height = floor($scale_by * $this->original_image->height);
            WP_CLI::log("Processing {$this->original_image->file}:");
            WP_CLI::log("Resizing to Width of $resize_to_width and height of $resize_to_height.");
           
        }
        
        else{
            WP_CLI::log("Width of {$this->original_image->width} and height of {$this->original_image->height} do not qualify for resize, but trying filter reduction to save space");
            $resize_to_width = $this->original_image->width;
            $resize_to_height = $this->original_image->height;
        }  
        
        $og_img = new \Imagick($this->original_image->file);
        $path_info = pathinfo($this->original_image->file);
        $bckup_file = "{$path_info['dirname']}/{$path_info['filename']}-original.{$path_info['extension']}";
        $og_img->writeImage($bckup_file);
                    
        $og_size = filesize($this->original_image->file);
        
        $this->smartResize($og_img, $resize_to_width, $resize_to_height, \Imagick::FILTER_POINT);
        
        $og_img->writeImage($this->original_image->file);
        
        Self::updateAttachmentMeta($this->attachment_id, $this->original_image->file);       
                            
        $rszd_size = filesize($this->original_image->file);
                    
        $saved = 100 - (int) $rszd_size / $og_size * 100;
        
        if($saved <= 0){
            //revert everything!!!!!!!
             $og_img = new \Imagick($bckup_file);
             $og_img->writeImage($this->original_image->file);
             Self::updateAttachmentMeta($this->attachment_id, $this->original_image->file); 
             WP_CLI::log("Resize didn't reduce size ({$saved}% saved), reverting to original");
        }
        else{
            WP_CLI::log("Resize complete. Reduced by {$saved}%");
        }     
        
        //delete backup file
        unlink($bckup_file);
    }
    
    public function resize($max_width, $max_height){
        //only handle jpg for now. and make sure the image exists
        
        if(!in_array($this->mime_type, ['image/jpeg']) || !file_exists($this->original_image->file)){
            //get outta dodge yo
            WP_CLI::log( sprintf( 'Skipping image %s...', $this->original_image->file ) );
            return false;
        }
        
        if($this->original_image->width > $max_width || $this->original_image->height > $max_height){
            
            WP_CLI::log("Width of {$this->original_image->width} and height of {$this->original_image->height} need resize.");
            
            //figure out if we're dealing with width or height
            $largest_dimension = $this->original_image->width > $this->original_image->height ? 'width' : 'height';
            
            //compute scale
            switch($largest_dimension){
                case 'width':
                    $scale_by = (float) $max_width / $this->original_image->width;
                break;               
                case 'height':
                    $scale_by = (float) $max_height / $this->original_image->height;
                break;                
            }
            
            //get new sizes
            $resize_to_width = floor($scale_by * $this->original_image->width);
            $resize_to_height = floor($scale_by * $this->original_image->height);
            WP_CLI::log("Processing {$this->original_image->file}:");
            
            $og_img = new \Imagick($this->original_image->file);
                        
            $og_size = filesize($this->original_image->file);
            
            $this->smartResize($og_img, $resize_to_width, $resize_to_height);
            
            $og_img->writeImage($this->original_image->file);
            
            Self::updateAttachmentMeta($this->attachment_id, $this->original_image->file);       
                                
            $rszd_size = filesize($this->original_image->file);
            
            $saved = 100 - (int) $rszd_size / $og_size * 100;
            WP_CLI::log("Resize complete. Reduced by {$saved}%");
        }
        
        else{
            WP_CLI::log("Width of {$this->original_image->width} and height of {$this->original_image->height} do not qualify for resize.");
        }
    }

  
    public function optimize($optimization_level = 95){

        //only handle jpg for now. and make sure the image exists
        
        if(!in_array($this->mime_type, ['image/jpeg', 'image/png']) || !file_exists($this->original_image->file)){
            //get outta dodge yo
            WP_CLI::log( sprintf( 'Skipping image Optimization for %s...', $this->original_image->file ) );
            return false;
        }
        
        $image_editor = wp_get_image_editor($this->original_image->file);
        if (is_wp_error($image_editor)) {
            throw new Exception("Issue creating image editor to resize");   
        }
        
        $current_optimization = $image_editor->get_quality();
        
        if($current_optimization > $optimization_level ){
            
            WP_CLI::log("Image quality of {$current_optimization} needs optimization.");
            
            $result = $image_editor->set_quality($optimization_level);
            if($result && !is_wp_error($result)) {
                $image_editor->save($this->original_image->file);
            };

            Self::updateAttachmentMeta($this->attachment_id, $this->original_image->file);  
                                                
            WP_CLI::log("Optimization complete.");            
        }
        
        else{
            WP_CLI::log("Optimization level of $current_optimization does not qualify for optimization.");
        }
        
    }
  
    public function stripMeta(){

        //only handle jpg for now. and make sure the image exists
        
        if(!in_array($this->mime_type, ['image/jpeg']) || !file_exists($this->original_image->file)){
            //get outta dodge yo
            WP_CLI::log( sprintf( 'Skipping image Meta Stripping for %s...', $this->original_image->file ) );
            return false;
        }
                
        $img = new \Imagick($this->original_image->file);
        //get icc meta to put back in after stripping.
        $profiles = $img->getImageProfiles("icc", true);

        if (!$img->stripImage()) {
            throw new Exception("Issue stripping image");   
        }

        WP_CLI::log("Image needs meta stripping");
            
        if(!empty($profiles)){
            //maintain icc meta
            $img->profileImage("icc", $profiles['icc']);
        }
        
        //save that sucker        
        $img->writeImage();
            
        Self::updateAttachmentMeta($this->attachment_id, $this->original_image->file);  
                                                
        WP_CLI::log("Image meta stripped");            
           
    }
    
    public function convertPng2Jpg(){
         if(!in_array($this->mime_type, ['image/png']) || !file_exists($this->original_image->file)){
            //get outta dodge yo
            WP_CLI::log( sprintf( 'Skipping image png2jpg conversion for %s...', $this->original_image->file ) );
            return false;
        }
        
        // Create instance of an image
        $imagick = new \Imagick();

        $imagick->readImage($this->original_image->file);

        // 0 = No transparency
        // 1 = Has transparency
        $hasTransparency = $imagick->getImageAlphaChannel();

        if($hasTransparency){
            WP_CLI::log("Attachment ID {$this->attachment_id} has alpha transparency, skip conversion to avoid losses");
            return false;
        }else{
            WP_CLI::log("Converting Attachment ID {$this->attachment_id} .jpg due to lack of transparency");
            //delete all attachments first
            WP_CLI::log("Deleting old .png subsizes");
            Self::getAndDeleteSubsizes($this->attachment_id, $this->path_info['dirname']);            
            $imagick->setImageFormat('jpeg');
            $path_info = pathinfo($this->original_image->file);
            $png_file = $this->original_image->file;
            $this->original_image->file = $jpg = "{$path_info['dirname']}/{$path_info['filename']}.jpg";
            $imagick->writeImage($jpg);
            WP_CLI::log("PNG $png_file converted to JPG $jpg");
            $this->updateAttachmentMeta($this->attachment_id, $this->original_image->file);
            //delete .png
            unlink($png_file);
            WP_CLI::log("PNG $png_file deleted");
            return true;
        }
    }
    
    public function regenerateSubsizes(){
         //only handle jpg for now. and make sure the image exists
        
        if(!in_array($this->mime_type, ['image/jpeg', 'image/png']) || !file_exists($this->original_image->file)){
            //get outta dodge yo
            WP_CLI::log( sprintf( 'Skipping image subsize regeneration for %s...', $this->original_image->file ) );
            return false;
        }
        
        Self::getAndDeleteSubsizes($this->attachment_id, $this->path_info['dirname']);
  
        /*
        * Custom filter to remove default image sizes from WordPress.
        */
         
        if(!has_filter('intermediate_image_sizes_advanced', '\ImageResize::removeDefaultImageSizes')){
            //WP_CLI::log("ADDING FILTER TO REMOVE DEFAULT IMAGE SIZES");
            add_filter('intermediate_image_sizes_advanced', '\ImageResize::removeDefaultImageSizes');
        }
            
        Self::updateAttachmentMeta($this->attachment_id, $this->original_image->file);
             
        if(has_filter('intermediate_image_sizes_advanced', '\ImageResize::removeDefaultImageSizes')){
            //WP_CLI::log("REMOVING FILTER TO REMOVE DEFAULT IMAGE SIZES");
            remove_filter( 'intermediate_image_sizes_advanced', '\ImageResize::removeDefaultImageSizes');
        }
        
        return true;
                
    }
    
    protected static function getAndDeleteSubsizes($attachment_id, $dirname){
        $attach_data = wp_get_attachment_metadata($attachment_id);
                
        foreach($attach_data['sizes'] as $subsize_image){
            $subsize_image_filepath = "{$dirname}/{$subsize_image['file']}";            
            wp_delete_file($subsize_image_filepath);
            WP_CLI::log("Deleted {$subsize_image_filepath}");
        }
        
        return true;
    }
    
    protected static function updateAttachmentMeta($attachment_id, $file){
        //in case filename/type has changed
        WP_CLI::log("Updating filename and meta for Attachment $attachment_id");
        
        update_attached_file($attachment_id, $file);
        $attach_data = wp_generate_attachment_metadata( $attachment_id, $file);        
        wp_update_attachment_metadata( $attachment_id, $attach_data );
        
        return true;
    }
    
    public static function removeDefaultImageSizes($sizes){
        
        /* Default WordPress */
        //unset( $sizes[ 'thumbnail' ]);     // Remove Thumbnail (150 x 150 hard cropped)
        //unset( $sizes[ 'medium' ]);          // Remove Medium resolution (300 x 300 max height 300px)
        unset( $sizes[ 'medium_large' ]);    // Remove Medium Large (added in WP 4.4) resolution (768 x 0 infinite height)
        unset( $sizes[ 'large' ]);           // Remove Large resolution (1024 x 1024 max height 1024px)
        unset( $sizes[ '1536x1536' ]);       // Remove Large resolution (1536 x 1536 max height 1536)
        unset( $sizes[ '2048x2048' ]);       // Remove Large resolution (2048 x 2048 max height 2048px)
        return $sizes;
    }
    
    
    
    /**
     * Resizes the image using smart defaults for high quality and low file size.
     *
     * This function is basically equivalent to:
     *
     * $optim == true: `mogrify -path OUTPUT_PATH -filter Triangle -define filter:support=2.0 -thumbnail OUTPUT_WIDTH -unsharp 0.25x0.08+8.3+0.045 -dither None -posterize 136 -quality 82 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB INPUT_PATH`
     *
     * $optim == false: `mogrify -path OUTPUT_PATH -filter Triangle -define filter:support=2.0 -thumbnail OUTPUT_WIDTH -unsharp 0.25x0.25+8+0.065 -dither None -posterize 136 -quality 82 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB -strip INPUT_PATH`
     *
     * @access  public
     *
     * @param   integer $columns        The number of columns in the output image. 0 = maintain aspect ratio based on $rows.
     * @param   integer $rows           The number of rows in the output image. 0 = maintain aspect ratio based on $columns.
     * @param   bool    $optim          Whether you intend to perform optimization on the resulting image. Note that setting this to `true` doesn’t actually perform any optimization.
     */    
    public function smartResize($img, $columns, $rows, $filter = \Imagick::FILTER_TRIANGLE, $optim = false, $compression_quality = 90) {

        $img->setOption('filter:support', '2.0');
        $this->thumbnailImage($img, $columns, $rows, true, false, $filter);
        if ($optim) {
            $img->unsharpMaskImage(0.25, 0.08, 8.3, 0.045);
        } else {
            $img->unsharpMaskImage(0.25, 0.25, 8, 0.065);
        }
        $img->posterizeImage(136, false);
        if($img->getImageCompressionQuality() > $compression_quality){
            $img->setImageCompressionQuality($compression_quality);
        }
        $img->setOption('jpeg:fancy-upsampling', 'off');
        $img->setOption('png:compression-filter', '5');
        $img->setOption('png:compression-level', '9');
        $img->setOption('png:compression-strategy', '1');
        $img->setOption('png:exclude-chunk', 'all');
        $img->setInterlaceScheme(\Imagick::INTERLACE_NO);
        $img->setColorspace(\Imagick::COLORSPACE_SRGB);
        if (!$optim) {
            $img->stripImage();
        }
    }   
        
    /**
     * Changes the size of an image to the given dimensions and removes any associated profiles.
     *
     * `thumbnailImage` changes the size of an image to the given dimensions and
     * removes any associated profiles.  The goal is to produce small low cost
     * thumbnail images suited for display on the Web.
     *
     * With the original Imagick thumbnailImage implementation, there is no way to choose a
     * resampling filter. This class recreates Imagick’s C implementation and adds this
     * additional feature.
     *
     * Note: <https://github.com/mkoppanen/imagick/issues/90> has been filed for this issue.
     *
     * @access  public
     *
     * @param   integer $columns        The number of columns in the output image. 0 = maintain aspect ratio based on $rows.
     * @param   integer $rows           The number of rows in the output image. 0 = maintain aspect ratio based on $columns.
     * @param   bool    $bestfit        Treat $columns and $rows as a bounding box in which to fit the image.
     * @param   bool    $fill           Fill in the bounding box with the background colour.
     * @param   integer $filter         The resampling filter to use. Refer to the list of filter constants at <http://php.net/manual/en/imagick.constants.php>.
     *
     * @return  bool    Indicates whether the operation was performed successfully.
     */

    public function thumbnailImage($img, $columns, $rows, $bestfit = false, $fill = false, $filter = \Imagick::FILTER_TRIANGLE) {

        // sample factor; defined in original ImageMagick thumbnailImage function
        // the scale to which the image should be resized using the `sample` function
        $SampleFactor = 5;

        // filter whitelist
        $filters = array(
            \Imagick::FILTER_POINT,
            \Imagick::FILTER_BOX,
            \Imagick::FILTER_TRIANGLE,
            \Imagick::FILTER_HERMITE,
            \Imagick::FILTER_HANNING,
            \Imagick::FILTER_HAMMING,
            \Imagick::FILTER_BLACKMAN,
            \Imagick::FILTER_GAUSSIAN,
            \Imagick::FILTER_QUADRATIC,
            \Imagick::FILTER_CUBIC,
            \Imagick::FILTER_CATROM,
            \Imagick::FILTER_MITCHELL,
            \Imagick::FILTER_LANCZOS,
            \Imagick::FILTER_BESSEL,
            \Imagick::FILTER_SINC
        );

        // Parse parameters given to function
        $columns = (double) ($columns);
        $rows = (double) ($rows);
        $bestfit = (bool) $bestfit;
        $fill = (bool) $fill;

        // We can’t resize to (0,0)
        if ($rows < 1 && $columns < 1) {
            return false;
        }

        // Set a default filter if an acceptable one wasn’t passed
        if (!in_array($filter, $filters)) {
            $filter = \Imagick::FILTER_TRIANGLE;
        }

        // figure out the output width and height
        $width = (double) ($img->getImageWidth());
        $height = (double) ($img->getImageHeight());
        $new_width = $columns;
        $new_height = $rows;

        $x_factor = $columns / $width;
        $y_factor = $rows / $height;
        if ($rows < 1) {
            $new_height = round($x_factor * $height);
        } elseif ($columns < 1) {
            $new_width = round($y_factor * $width);
        }

        // if bestfit is true, the new_width/new_height of the image will be different than
        // the columns/rows parameters; those will define a bounding box in which the image will be fit
        if ($bestfit && $x_factor > $y_factor) {
            $x_factor = $y_factor;
            $new_width = round($y_factor * $width);
        } elseif ($bestfit && $y_factor > $x_factor) {
            $y_factor = $x_factor;
            $new_height = round($x_factor * $height);
        }
        if ($new_width < 1) {
            $new_width = 1;
        }
        if ($new_height < 1) {
            $new_height = 1;
        }

        // if we’re resizing the image to more than about 1/3 it’s original size
        // then just use the resize function
        if (($x_factor * $y_factor) > 0.1) {
            $img->resizeImage($new_width, $new_height, $filter, 1);

        // if we’d be using sample to scale to smaller than 128x128, just use resize
        } elseif ((($SampleFactor * $new_width) < 128) || (($SampleFactor * $new_height) < 128)) {
                $img->resizeImage($new_width, $new_height, $filter, 1);

        // otherwise, use sample first, then resize
        } else {
            $img->sampleImage($SampleFactor * $new_width, $SampleFactor * $new_height);
            $img->resizeImage($new_width, $new_height, $filter, 1);
        }

        // if the alpha channel is not defined, make it opaque
        // if ($img->getImageAlphaChannel() == \Imagick::ALPHACHANNEL_UNDEFINED) {
        //     $img->setImageAlphaChannel(\Imagick::ALPHACHANNEL_OPAQUE);
        // }

        // set the image’s bit depth to 8 bits
        $img->setImageDepth(8);

        // turn off interlacing
        $img->setInterlaceScheme(\Imagick::INTERLACE_NO);
        
        // Strip all profiles except color profiles.
        foreach ($img->getImageProfiles('*', true) as $key => $value) {
            if ($key != 'icc' && $key != 'icm') {
                try{
                    $img->removeImageProfile($key);
                }
                catch(\Exception $e){
                    WP_CLI::log("ERROR STRIPPING PROFILE: $key => $value on image {$this->attachment_id}");
                    //carry on wayward solider
                }
            }
        }

        if (method_exists($img, 'deleteImageProperty')) {
            $img->deleteImageProperty('comment');
            $img->deleteImageProperty('Thumb::URI');
            $img->deleteImageProperty('Thumb::MTime');
            $img->deleteImageProperty('Thumb::Size');
            $img->deleteImageProperty('Thumb::Mimetype');
            $img->deleteImageProperty('software');
            $img->deleteImageProperty('Thumb::Image::Width');
            $img->deleteImageProperty('Thumb::Image::Height');
            $img->deleteImageProperty('Thumb::Document::Pages');
        } else {
            $img->setImageProperty('comment', '');
            $img->setImageProperty('Thumb::URI', '');
            $img->setImageProperty('Thumb::MTime', '');
            $img->setImageProperty('Thumb::Size', '');
            $img->setImageProperty('Thumb::Mimetype', '');
            $img->setImageProperty('software', '');
            $img->setImageProperty('Thumb::Image::Width', '');
            $img->setImageProperty('Thumb::Image::Height', '');
            $img->setImageProperty('Thumb::Document::Pages', '');
        }

        // In case user wants to fill use extent for it rather than creating a new canvas
        // …fill out the bounding box
        if ($bestfit && $fill && ($new_width != $columns || $new_height != $rows)) {
            $extent_x = 0;
            $extent_y = 0;

            if ($columns > $new_width) {
                $extent_x = ($columns - $new_width) / 2;
            }
            if ($rows > $new_height) {
                $extent_y = ($rows - $new_height) / 2;
            }

            $img->extentImage($columns, $rows, 0 - $extent_x, $extent_y);
        }

        return true;

    }
    
    
}